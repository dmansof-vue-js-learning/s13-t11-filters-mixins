import Vue from 'vue'
import App from './App.vue'

Vue.filter('append-length', function(value) {
    return value != '' ? `${value} (${value.length})` : value;
});

Vue.filter('to-lowercase', function(value) {
    return value.toLowerCase();
});

Vue.mixin({
    computed: {
        reversedTextMixin: function() {
            const text = this.sampleText;
            return text != '' ? text.split('').reverse().join('') : value;
        },
        withLengthTextMixin: function() {
            const text = this.sampleText;
            return text != '' ? `${text} (${text.length})` : value;
        }
    }
});

new Vue({
  el: '#app',
  render: h => h(App)
})
